{ input }:
let
  inherit (builtins) elem elemAt filter foldl' genList head length map split stringLength tail;
  inherit (import ../utils.nix) splitChar splitLines splitWords toInt;

  puzzle = let
    plan = split "\n\n" input;
  in { "stacks" = elemAt plan 0; "actions" = splitLines (elemAt plan 2); };

  stacks = let
    stackLines = splitLines puzzle.stacks;
    linesCount = (length stackLines) - 1;
    lineLength = stringLength (elemAt stackLines 0);
    stacksCount = (lineLength + 1) / 4;
    chars = filter (x: x!= "\n") (splitChar puzzle.stacks);
    stack = i: genList (x: elemAt chars (x * lineLength + (1 + 4 * i))) linesCount;
  in map (s: filter (x: x != " ") s) (genList stack stacksCount);

  actions = let
    actionsWords = map (l: splitWords l) puzzle.actions;
    pos = x: (toInt x) - 1;
  in map (x: [(toInt (elemAt x 1)) (pos (elemAt x 3)) (pos (elemAt x 5))]) actionsWords;

  rearranged = let
    move = stack: r: o: d: genList (i:
      if i == o
      then genList (x: elemAt (elemAt stack o) (x + r)) ((length (elemAt stack o)) - r)
      else if i == d
      then (genList (x: elemAt (elemAt stack o) x) r) ++ (elemAt stack d)
      else elemAt stack i)
    (length stack);
  in foldl' (s: a: move s (elemAt a 0) (elemAt a 1) (elemAt a 2)) stacks actions;

in foldl' (x: y: x + y) "" (map head rearranged)

