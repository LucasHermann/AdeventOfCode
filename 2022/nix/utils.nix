let
  inherit (builtins) all attrNames elem filter foldl' head isString lessThan map split sort tail;
in rec {

  # Splitting helpers
  splitX = spliter: s: filter (x: isString x && x != "") (split spliter s);
  splitChar = splitX "";
  splitLines = splitX "\n";
  splitWords = splitX " ";

  digitDict = {
    "0" = 0;
    "1" = 1;
    "2" = 2;
    "3" = 3;
    "4" = 4;
    "5" = 5;
    "6" = 6;
    "7" = 7;
    "8" = 8;
    "9" = 9;
  };

  isDigit = x: elem x (attrNames digitDict);
  isInt = x: all isDigit (splitChar x);

  toInt = string: let
    chars = splitChar string;
    num = xs: foldl' (a: b: 10*a + digitDict.${b}) 0 xs;
  in if head chars == "-" then num (tail chars) * -1 else num chars;

  toIntFromBit = string: let
    binDict = {
      "0" = 0;
      "1" = 1;
    };
  in foldl' (x: y: 2 * x + binDict.${y}) 0 (splitChar string);

  max = x: y: if x > y then x else y;
  min = x: y: if x < y then x else y;

  abs = x: if x > 0 then x else x * -1;

  incOrder = xs: sort lessThan xs;
  decOrder = xs: sort (x: y: x > y) xs;

  _innerEuclidDiv = quotient: dividend: divisor: if dividend  < divisor then { quo = quotient; rem = dividend; div = divisor; } else _innerEuclidDiv (quotient + 1) (dividend - divisor) divisor;
  euclidDiv = x: d: _innerEuclidDiv 0 x d;

  round = { quo, rem, div }: if rem > (div / 2) then quo + 1 else quo;
  sum = xs: foldl' (x: y: x + y) 0 xs;
}

