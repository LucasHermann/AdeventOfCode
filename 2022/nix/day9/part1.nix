{ input }:
let
  inherit (builtins) attrNames concatMap elem elemAt foldl' length genericClosure groupBy map;
  inherit (import ../utils.nix) abs max splitLines splitWords;
  inherit (import ./utils.nix) parse;

  puzzle = concatMap (l: parse (splitWords l)) (splitLines input);

  root = { x = 0; y = 0; };

  distance = h: t: abs (h - t);

  follow = h: t: let
    distX = distance h.x t.x;
    distY = distance h.y t.y;
    newPos = h: t: if h - t == 0 then t else if h - t < 0 then t - 1 else t + 1;
  in if max distX distY <= 1 then t else { x = newPos h.x t.x; y = newPos h.y t.y; };

  positions = genericClosure {
    startSet = [ {key = -1; head = root; tail = root;} ];
    operator = item: let
      nextKey = item.key + 1;
      nextHead = elemAt puzzle nextKey item.head;
      nextTail = follow nextHead item.tail;
    in [{
      key = if nextKey < length puzzle then nextKey else item.key;
      head = nextHead;
      tail = nextTail;
    }];
  };

  deDup = xs: groupBy (x: (toString (x.x)) + "-" + (toString (x.y))) xs;

in length (attrNames (deDup (map (x: x.tail) positions)))

