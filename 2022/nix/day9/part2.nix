{ input }:
let
  inherit (builtins) attrNames concatMap elemAt length genericClosure getAttr groupBy map mapAttrs;
  inherit (import ../utils.nix) abs max splitLines splitWords;
  inherit (import ./utils.nix) parse;

  puzzle = concatMap (l: parse (splitWords l)) (splitLines input);

  root = { x = 0; y = 0; };
  initialTail = {
    "0" = root;
    "1" = root;
    "2" = root;
    "3" = root;
    "4" = root;
    "5" = root;
    "6" = root;
    "7" = root;
    "8" = root;
  };

  distance = h: t: abs (h - t);

  follow = h: t: let
    distX = distance h.x t.x;
    distY = distance h.y t.y;
    newPos = h: t: if h - t == 0 then t else if h - t < 0 then t - 1 else t + 1;
  in if max distX distY <= 1 then t else { x = newPos h.x t.x; y = newPos h.y t.y; };

  updateTail = head: prev: mapAttrs (n: v: (elemAt v 0).pos) (groupBy (x: toString x.key) (genericClosure {
    startSet = [ {key = 0; pos = follow head prev."0";} ];
    operator = item: let
      nextKey = item.key + 1;
      nextPos = follow item.pos (getAttr (toString nextKey) prev);
    in [{
      key = if nextKey < 9 then nextKey else item.key;
      pos = nextPos;
    }];
  }));

  positions = genericClosure {
    startSet = [ {key = -1; head = root; tail = initialTail ;} ];
    operator = item: let
      nextKey = item.key + 1;
      nextHead = elemAt puzzle nextKey item.head;
      nextTail = updateTail nextHead item.tail;
    in [{
      key = if nextKey < length puzzle then nextKey else item.key;
      head = nextHead;
      tail = nextTail;
    }];
  };

  deDup = xs: groupBy (x: (toString (x.x)) + "-" + (toString (x.y))) xs;

# in positions
# in (map (x: x.tail."8") positions)
in length (attrNames (deDup (map (x: x.tail."8") positions)))

