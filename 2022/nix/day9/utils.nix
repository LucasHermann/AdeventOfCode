let
  inherit (builtins) elemAt genList getAttr;
  inherit (import ../utils.nix) toInt;
in rec {
  moveUp = head: head // { y = head.y + 1; };
  moveDown = head: head // { y = head.y - 1; };
  moveLeft = head: head // { x = head.x - 1; };
  moveRight = head: head // { x = head.x + 1; };

  parse = let
    dirActions = {
      "U" = moveUp;
      "D" = moveDown;
      "L" = moveLeft;
      "R" = moveRight;
    };
  in l: genList (x: getAttr (elemAt l 0) dirActions) (toInt (elemAt l 1));

}

