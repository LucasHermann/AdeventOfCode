{ input }:
let
  inherit (builtins) elemAt map;
  inherit (import ../utils.nix) decOrder sum splitLines splitX toInt;
  inherit (import ./utils.nix) parseInput;

  puzzle = parseInput input;

  calories = map sum puzzle;

in elemAt (decOrder calories) 0 + elemAt (decOrder calories) 1 + elemAt (decOrder calories) 2 

