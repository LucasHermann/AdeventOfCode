{ input }:
let
  inherit (builtins) elemAt foldl' genList length map partition;
  inherit (import ../utils.nix) splitChar splitLines;

  puzzle = splitChar (elemAt (splitLines input) 0);

  isUnique = x: xs: (length (partition (y: y == x) xs).right) == 1;
  isPacketMarker = x: xs: foldl' (x: y: x && y) true (map (x: isUnique x xs) xs);
  window = xs: i: genList (x: elemAt xs (i - x)) 14;
  markers = xs: genList (x: isPacketMarker (elemAt xs (x + 14)) (window xs (x + 14))) ((length xs) - 14);

  firstMarker = i: xs: if elemAt xs i then i + 1 else firstMarker (i + 1) xs;

in (firstMarker 0 (markers puzzle)) + 14

