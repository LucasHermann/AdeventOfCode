{ input }:
let
  inherit (builtins) elem elemAt genList getAttr length map;
  inherit (import ../utils.nix) sum splitChar splitLines;
  inherit (import ./utils.nix) toPriority;

  puzzle = map splitChar (splitLines input);

  pack = let
    firstComp = xs: l: genList (x: elemAt xs x) l;
    secondComp = xs: l: genList (x: elemAt xs (l + x)) l;
    compartiment = xs: l: { "first" = firstComp xs l; "second" = secondComp xs l; };
  in xs: compartiment xs ((length xs) / 2);
  packs = map pack puzzle;

  findError = xs: ys: i: if elem (elemAt xs i) ys then elemAt xs i else findError xs ys (i + 1);
  error = { first, second }: findError first second 0;

  priorities = map (xs: toPriority (error xs)) packs;

in sum priorities

