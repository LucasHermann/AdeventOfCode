{ input }:
let
  inherit (builtins) elem elemAt genList getAttr length map;
  inherit (import ../utils.nix) sum splitChar splitLines;
  inherit (import ./utils.nix) toPriority;

  puzzle = map splitChar (splitLines input);

  groups = genList (x: {
    "first" = elemAt puzzle (x * 3);
    "second" = elemAt puzzle ((x * 3) + 1);
    "third" = elemAt puzzle ((x * 3) + 2);
  }) ((length puzzle) / 3);
  groupBadge = let
    findBadge = f: s: t: i: if elem (elemAt f i) s && elem (elemAt f i) t then elemAt f i else findBadge f s t (i + 1);
  in { first, second, third }: findBadge first second third 0;

in sum (map (xs: toPriority (groupBadge xs)) groups)

