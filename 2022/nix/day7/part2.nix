{ input }:
let
  inherit (builtins) attrValues concatStringsSep elemAt filter foldl' genList getAttr hasAttr head length listToAttrs map mapAttrs seq tail;
  inherit (import ../utils.nix) incOrder splitLines splitWords splitX sum toInt;
  inherit (import ./utils.nix) dir file;

  formatted = let
    formatter = xs: { "cmd" = head xs; "args" = tail xs; };
  in xs: if (length xs) == 1 then formatter (splitWords (elemAt xs 0)) else formatter xs;
  puzzle = map (x: formatted (splitLines x)) (splitX "\\$ " input);

  root = dir "root" [(dir "/" [])];
  location = { "path" = []; "current" = root; };

  state = { path = []; elements = { "/" = root; }; };

  lsBis = let
    fullPath = path: concatStringsSep "/" path;
    newElem = e: path: if (elemAt e 0) == "dir"
    then { name = path + "/" + elemAt e 1; value = dir (path + "/" + (elemAt e 1)) []; }
    else { name = path + "/" + elemAt e 1; value = file ( path +  "/" +(elemAt e 1)) (toInt (elemAt e 0)); };
 in state: args: state // { elements = state.elements // { "${fullPath state.path}" = listToAttrs (map (c: newElem (splitWords c) (fullPath state.path)) args); }; };

  cdBis = state: args: if (elemAt args 0) == ".."
  then state // { elements = state.elements; path = genList (x: elemAt state.path x) ((length state.path) - 1); }
  else state // { elements = state.elements; path = state.path ++ [(elemAt args 0)]; };

  commands = {
    ls = lsBis;
    cd = cdBis;
  };
  exec = state: { cmd, args }: getAttr cmd commands state args;

  getSize = let
  in state: t: if hasAttr "childs" t
  then foldl' (x: y: x + (getSize state y)) 0 (attrValues (getAttr t.name state.elements))
  else t.size;

  finalState = foldl' (x: y: exec x y) state puzzle;
  dirsSize = mapAttrs (n: v: mapAttrs (nn: vv: getSize finalState vv) v) finalState.elements;
  usedSpace = sum (attrValues dirsSize."/");
  toFree = usedSpace - 40000000;

# in sum (filter (x: 100000 >= x) (attrValues (mapAttrs (n: v:  sum (attrValues v)) dirsSize)))
in head (incOrder (filter (x: x >= toFree) (map (d: (sum (attrValues d))) (attrValues dirsSize))))

