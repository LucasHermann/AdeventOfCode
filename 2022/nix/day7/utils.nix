let
  inherit (builtins) foldl' map;
  # inherit (import ../utils.nix) splitLines splitX toInt;
in rec {

  file = let
    fileFunc = { __functor = self: t: t + self.size; };
  in n: s: fileFunc // { "name" = n; "size" = s; };

  dir = let
    dirFunc = { __functor = self: t: t + foldl' (x: y: y x) t self.childs; };
  in n: c: dirFunc // { "name" = n; "childs" = c; };

}

