{ input }:
let
  inherit (builtins) elemAt head concatLists foldl' filter genList length tail;
  inherit (import ../utils.nix) decOrder splitChar splitLines toInt;

  puzzle = map (l: map toInt (splitChar l)) (splitLines input);

  lines = puzzle;
  linesLength = length (elemAt lines 0);
  columns = genList (i: genList (x: elemAt (elemAt puzzle x) i) (length puzzle)) linesLength;
  columnsLength = length (elemAt columns 0);

  viewDist = let
    innerVis = side: t: r: if (length side) > 1 && t > (head side)
    then innerVis (tail side) t (r+1)
    else r + 1;
  in {before, after, tree}: innerVis before tree 0 * innerVis after tree 0;
  format = xs: i: {
    before = genList (x: elemAt xs (i - x - 1)) i;
    after = genList (x: elemAt xs (x+i+1)) ((length xs) - i - 1);
    tree = elemAt xs i;
  };

  lineVis = let
    line = l: elemAt lines (l + 1);
  in genList (l: genList (t: viewDist (format (line l) (t + 1))) (linesLength - 2)) ((length lines) - 2);
  colVis = let
    column = c: elemAt columns (c + 1);
  in genList (c: genList (t: viewDist (format (column c) (t + 1))) (columnsLength - 2)) ((length columns) - 2);
  treeVis = concatLists (genList (li: genList (ci: ((elemAt (elemAt lineVis li) ci) * (elemAt (elemAt colVis ci) li))) (length colVis)) (length lineVis));

# in treeVis
in head (decOrder treeVis)

