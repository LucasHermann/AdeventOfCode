{ input }:
let
  inherit (builtins) elemAt concatLists foldl' filter genList length;
  inherit (import ../utils.nix) splitChar splitLines toInt;

  puzzle = map (l: map toInt (splitChar l)) (splitLines input);

  lines = puzzle;
  linesLength = length (elemAt lines 0);
  columns = genList (i: genList (x: elemAt (elemAt puzzle x) i) (length puzzle)) linesLength;
  columnsLength = length (elemAt columns 0);

  isVis = let
    innerVis = side: t: foldl' (x: y: x && (t > y)) true side;
  in {before, after, tree}: innerVis before tree || innerVis after tree;
  format = xs: i: {
    before = genList (x: elemAt xs x) i;
    after = genList (x: elemAt xs (x+i+1)) ((length xs) - i - 1);
    tree = elemAt xs i;
  };

  lineVis = let
    line = l: elemAt lines (l + 1);
  in genList (l: genList (t: isVis (format (line l) (t + 1))) (linesLength - 2)) ((length lines) - 2);
  colVis = let
    column = c: elemAt columns (c + 1);
  in genList (c: genList (t: isVis (format (column c) (t + 1))) (columnsLength - 2)) ((length columns) - 2);
  treeVis = concatLists (genList (li: genList (ci: ((elemAt (elemAt lineVis li) ci) || (elemAt (elemAt colVis ci) li))) (length colVis)) (length lineVis));

in (linesLength * 2) + ((columnsLength - 2) * 2) + length (filter (x: x) treeVis)

