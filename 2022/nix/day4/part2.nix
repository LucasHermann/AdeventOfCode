{ input }:
let
  inherit (builtins) elemAt foldl' map;
  inherit (import ../utils.nix) splitLines splitX toInt;

  puzzle = map (x: map (y: map toInt (splitX "-" y)) (splitX "," x)) (splitLines input);

  overlap = xs: if (elemAt (elemAt xs 0) 0) >= (elemAt (elemAt xs 1) 0)
  then (elemAt (elemAt xs 0) 0) <= (elemAt (elemAt xs 1) 1)
  else (elemAt (elemAt xs 0) 1) >= (elemAt (elemAt xs 1) 0);

in foldl' (x: y: if (overlap y) then x + 1 else x) 0 puzzle

