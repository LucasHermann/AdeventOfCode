{ input }:
let
  inherit (builtins) elemAt filter genList length map sort;
  inherit (import ../utils.nix) splitChar splitLines;
  inherit (import ./utils.nix) compareVals parse;

  puzzle = map parse (map splitChar (splitLines input));

  sortedSignals = sort (x: y: compareVals { left = x; right = y; }) (puzzle ++ [ [[2]] [[6]] ]);

  dividersPos = filter (x: x > 0) (genList (x: if elemAt sortedSignals x == [[2]] || elemAt sortedSignals x == [[6]] then (x + 1) else 0) (length sortedSignals));

in (elemAt dividersPos 0) * (elemAt dividersPos 1)

