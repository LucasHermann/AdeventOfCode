let
  inherit (builtins) elemAt getAttr length trace typeOf;
  inherit (import ../utils.nix) isDigit toInt;
in rec {

  parse = xs: let
    val = i: elemAt xs i;
    parseNum = i: let
      _innerParse = {res, i}: if isDigit (val i)
        then trace "Parsing digit at index ${toString i}" _innerParse { res = res + (val i); i = i + 1; }
        else trace "Finished parsing number at index ${toString i}" { res = toInt res; i = i; };
    in _innerParse { res = ""; i = i; };
    parseList = i: let
      _innerParse = {res, i}: if (val i) == "]"
        then trace "Finished parsing list at index ${toString i}" { res = res; i = i + 1; }
        else if (val i) == ","
        then trace "Skipping comma at index ${toString i}" _innerParse { res = res; i = i + 1; }
        else if (val i) == "["
        then trace "Parsing list starting at index ${toString i}" (if length res == 0
          then _innerParse { res = [(parseList (i + 1)).res]; i = (parseList (i + 1)).i; }
          else _innerParse { res = res ++ [(parseList (i + 1)).res]; i = (parseList (i + 1)).i; })
        else trace "parsing number starting at index ${toString i}" _innerParse { res = res ++ [(parseNum i).res]; i = (parseNum i).i; };
    in _innerParse { res = []; i = i; };
  in (parseList 1).res;

  compInts = {left, right}: if left == right then "skip" else left < right;

  compLists = i: {left, right}: let
    _innerComp = if i == (length left) && i == (length right)
    then trace "Both lists ends at the same time" "stop"
    else if i == (length left)
    then trace "Left list ends first" true
    else if i == (length right)
    then trace "Rigth list ends first" false
    else compareVals { left = elemAt left i; right = elemAt right i; };
  in trace "Comparing values ${toString i}" (if _innerComp == "skip"
  then trace "Moving to the next values" compLists (i + 1) { left = left; right = right; }
  else if _innerComp == "stop"
  then trace "No concluant result skipping" "skip"
  else trace "Got result" _innerComp);

  compareVals = {left, right}: let
    compDict = {
      "listlist" = trace "Both values are Lists" compLists 0 {left = left; right = right;};
      "listint" = trace "Left is a List while Right is an Int" compLists 0 {left = left; right = [right];};
      "intlist" = trace "Left is a Int while Right is an List" compLists 0 {left = [left]; right = right;};
      "intint" = trace "Both values are Ints" compInts {left = left; right = right;};
    };
  in getAttr ((typeOf left) + (typeOf right)) compDict;
}

