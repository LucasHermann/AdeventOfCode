{ input }:
let
  inherit (builtins) elemAt foldl' genList length map;
  inherit (import ../utils.nix) sum splitChar splitLines;
  inherit (import ./utils.nix) compareVals parse;

  puzzle = map parse (map splitChar (splitLines input));

  pairs = genList (x: { left = elemAt puzzle (x * 2); right = elemAt puzzle ((x * 2) + 1); }) ((length puzzle) / 2);

  orderedPairs = map compareVals pairs;

# in puzzle
# in pairs
in sum (genList (x: if elemAt orderedPairs x then (x + 1) else 0) (length orderedPairs))

