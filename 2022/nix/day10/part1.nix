{ input }:
let
  inherit (builtins) attrNames concatMap elem elemAt foldl' genericClosure getAttr groupBy hasAttr length map mapAttrs;
  inherit (import ../utils.nix) splitLines toInt;
  inherit (import ./utils.nix) parse;

  puzzle = map parse (splitLines input);

  run = mapAttrs (n: v: (elemAt v 0).rx) (groupBy (x: toString x.cycle) (genericClosure {
    startSet = [ {key = -1; cycle = 0; rx = 1;} ];
    operator = item: let
      nextKey = item.key + 1;
      exec = elemAt puzzle nextKey item;
    in [{
      key = if nextKey < length puzzle then nextKey else -1;
      cycle = exec.cycle;
      rx = exec.rx;
    }];
  }));

  getRx = cycle: if hasAttr cycle run then getAttr cycle run else getRx (toString ((toInt cycle) - 1));

# in puzzle
# in run
# in getRx "220"
in foldl' (x: y: x + ((getRx (toString ((toInt y) -1))) * (toInt y))) 0 ["20" "60" "100" "140" "180" "220"]

