{ input }:
let
  inherit (builtins) attrNames concatMap concatStringsSep elem elemAt foldl' genericClosure genList getAttr groupBy hasAttr length map mapAttrs;
  inherit (import ../utils.nix) splitLines toInt;
  inherit (import ./utils.nix) parse;

  puzzle = map parse (splitLines input);

  run = mapAttrs (n: v: (elemAt v 0).rx) (groupBy (x: toString x.cycle) (genericClosure {
    startSet = [ {key = -1; cycle = 0; rx = 1;} ];
    operator = item: let
      nextKey = item.key + 1;
      exec = elemAt puzzle nextKey item;
    in [{
      key = if nextKey < length puzzle then nextKey else -1;
      cycle = exec.cycle;
      rx = exec.rx;
    }];
  }));

  getRx = cycle: if hasAttr cycle run then getAttr cycle run else getRx (toString ((toInt cycle) - 1));

  sprite = cycle: let
    spriteCenter = getRx cycle;
  in [(spriteCenter - 1) spriteCenter (spriteCenter + 1)];

  drawLine = offset: let
    cycle = pixel: toString (offset + pixel);
    drawPixel = pixel: if elem pixel (sprite (cycle pixel)) then "#" else ".";
  in genList (pixel: drawPixel pixel) 40;

  drawScreen = genList (x: concatStringsSep "" (drawLine (x * 40))) 6;

in concatStringsSep "\n" drawScreen

