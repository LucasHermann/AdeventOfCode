let
  inherit (builtins) elemAt genList getAttr substring;
  inherit (import ../utils.nix) splitWords toInt;
in rec {
  noop = s: s // { cycle = s.cycle + 1; };
  addX = x: s: s // { cycle = s.cycle + 2; rx = s.rx + x; };

  parse = l: let
    instructions = {
      "noop" = noop;
      "addx" = addX (toInt (elemAt (splitWords l) 1));
    };
  in getAttr (substring 0 4 l) instructions;

}

