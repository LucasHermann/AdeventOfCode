let
  inherit (builtins) elemAt filter genList getAttr length trace;
  inherit (import ../utils.nix) abs;
in rec {

  getElevation = x: let
    elevationDict = {
      "S" = 0;
      "a" = 0;
      "b" = 1;
      "c" = 2;
      "d" = 3;
      "e" = 4;
      "f" = 5;
      "g" = 6;
      "h" = 7;
      "i" = 8;
      "j" = 9;
      "k" = 10;
      "l" = 11;
      "m" = 12;
      "n" = 13;
      "o" = 14;
      "p" = 15;
      "q" = 16;
      "r" = 17;
      "s" = 18;
      "t" = 19;
      "u" = 20;
      "v" = 21;
      "w" = 22;
      "x" = 23;
      "y" = 24;
      "z" = 25;
      "E" = 25;
    };
  in getAttr x elevationDict;

  isAccessible = grid: x: y: ((getPoint grid y) - (getPoint grid x)) <= 1;
  getPoint = grid: {x, y}: elemAt (elemAt grid y) x;
  getNeighbours = grid: pos: let
    verN = if pos.x == ((length (elemAt grid 0)) - 1)
      then [{x = pos.x - 1; y = pos.y;}]
      else if pos.x == 0 then [{x = pos.x + 1; y = pos.y;}]
      else [{x = pos.x - 1; y = pos.y;} {x = pos.x + 1; y = pos.y;}];
    horN = if pos.y == ((length grid) - 1)
      then [{x = pos.x; y = pos.y - 1;}]
      else if pos.y == 0 then [{x = pos.x; y = pos.y + 1;}]
      else [{x = pos.x; y = pos.y - 1;} {x = pos.x; y = pos.y + 1;}];
  in filter (x: isAccessible grid pos x) (verN ++ horN);
}

