{ input }:
let
  inherit (builtins) attrNames attrValues concatLists concatMap concatStringsSep elem elemAt filter genericClosure genList groupBy hasAttr head isAttrs length trace;
  inherit (import ../utils.nix) decOrder incOrder splitChar splitLines toInt;
  inherit (import ./utils.nix) getElevation getNeighbours getPoint;

  puzzle = map (x: map (y: [y (getElevation y)]) (splitChar x)) (splitLines input);

  isPoint = pos: p: if getPoint puzzle pos == p then pos else false;
  findPoint = p: filter isAttrs (concatLists (genList (x: genList (y: (isPoint {x=x; y=y;} p)) (length puzzle)) (length (elemAt puzzle 0))));

  starts = (findPoint ["S" 0]) ++ (findPoint ["a" 0]);
  end = elemAt (findPoint ["E" 25]) 0;
  grid = map (x: map (y: elemAt y 1) x) puzzle;

  paths = start: genericClosure {
    startSet = [{ key = 0; positions = [start]; visited = []; }];
    operator = item: let
      newVisited = item.visited ++ item.positions;
      isVisited = x: !(elem x newVisited);
      deDup = xs: map (x: elemAt x 0) (attrValues (groupBy (x: (toString x.x) + (toString x.y)) xs));
      nextPos = filter isVisited (deDup (concatMap (pos: getNeighbours grid pos) item.positions));
      nextKey = if elem end item.positions || (length nextPos == 0) || item.key == 600 then 0 else item.key + 1;
      log = x: let
        logPos = xs: concatStringsSep " " (map ({x, y}: "[${toString x}, ${toString y}]") xs);
      in trace "Step ${toString item.key}: \nCurrently at:\n${logPos item.positions}\nGoing to:\n${logPos nextPos}" x;
    in log [{key = nextKey; positions = nextPos; visited = newVisited;}];
  };

  # Print map of all visited points
  visualizePath = let
    visited = (elemAt (groupBy (x: toString x.key) paths)."600" 0).visited;
  in concatStringsSep "\n" (genList (x: concatStringsSep "" (genList (y: if elem {x=y;y=x;} visited then "#" else".") (length (elemAt grid 0)))) (length grid));

  pathLength = p: head (decOrder (map toInt (attrNames (groupBy (x: toString x.key) p))));

# in visualizePath
# in starts
in head (incOrder (map pathLength (map paths starts)))
# in map (x: map pathLength (paths x)) starts
# in head (decOrder (map toInt (attrNames (groupBy (x: toString x.key) paths))))

