{ input }:
let
  inherit (builtins) attrNames attrValues concatMap elem elemAt foldl' length genericClosure groupBy map mapAttrs trace zipAttrsWith;
  inherit (import ../utils.nix) decOrder sum;
  inherit (import ./utils.nix) parse procRound items;

  puzzle = parse input;

  rounds = genericClosure {
    startSet = [{ key = 0; monkeys = puzzle; }];
    operator = item: let
      newKey = if item.key == 10000 then 0 else item.key + 1;
      newMonkeys = procRound item.monkeys item.key;
    in [{ key = newKey; monkeys = newMonkeys; }];
  };

  inspections = let
    monkeyRounds = map (x: x.monkeys) rounds;
  in decOrder (attrValues (zipAttrsWith (n: v: elemAt (map (x: x.inspectionCounter) v) 10000) monkeyRounds));

# in inspections
in (elemAt inspections 0) * (elemAt inspections 1)

