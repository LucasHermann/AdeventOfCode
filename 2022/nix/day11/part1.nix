{ input }:
let
  inherit (builtins) attrNames attrValues concatMap elem elemAt foldl' length genericClosure groupBy map mapAttrs trace zipAttrsWith;
  inherit (import ../utils.nix) decOrder sum;
  inherit (import ./utils.nix) parse procRound2 items;

  puzzle = parse input;

  rounds = genericClosure {
    startSet = [{ key = 0; monkeys = puzzle; }];
    operator = item: let
      newKey = if item.key == 20 then 0 else item.key + 1;
      newMonkeys = procRound2 item.monkeys item.key;
    in [{ key = newKey; monkeys = newMonkeys; }];
  };

  inspections = let
    monkeyRounds = map (x: x.monkeys) rounds;
  in decOrder (attrValues (zipAttrsWith (n: v: elemAt (map (x: x.inspectionCounter) v) 20) monkeyRounds));

  itemsList = items puzzle;

  getOwnersNames = xs: map (y: y.monkey.name) xs;

  itemsOwners = let
  in decOrder (map length (attrValues (groupBy (x: toString x) (concatMap getOwnersNames itemsList))));

  item1 = mapAttrs (n: v: map (x: [x.value x.monkey]) v) (groupBy (x: toString x.round) (elemAt itemsList 0));
  foo = x: mapAttrs (n: v: length v) (groupBy (x: toString x) (map (x: x.monkey.name) x));

# in procRound puzzle
# in rounds
# in itemsOwners
in (elemAt inspections 0) * (elemAt inspections 1)
# in (elemAt itemsOwners 0) * (elemAt itemsOwners 1)
# in elemAt item1 ((length item1) - 1)
# in item1."1"
# in length (elemAt itemsList 0)
# in map sum (attrValues (zipAttrsWith (n: v: v) (map foo itemsList)))
# in foo (elemAt itemsList 0)
# in foo (elemAt itemsList 1)
# in foo (elemAt itemsList 2)
# in foo (elemAt itemsList 3)
# in foo (elemAt itemsList 4)
# in foo (elemAt itemsList 5)
# in foo (elemAt itemsList 6)
# in foo (elemAt itemsList 7)
# in foo (elemAt itemsList 8)
# in foo (elemAt itemsList 9)
# in foo (elemAt itemsList 10)
# in foo (elemAt itemsList 11)
# in foo (elemAt itemsList 12)
# in foo (elemAt itemsList 13)
# in foo (elemAt itemsList 14)
# in foo (elemAt itemsList 15)
# in foo (elemAt itemsList 16)
# in foo (elemAt itemsList 17)
# in foo (elemAt itemsList 18)
# in foo (elemAt itemsList 19)
# in foo (elemAt itemsList 20)
# in foo (elemAt itemsList 21)
# in foo (elemAt itemsList 22)
# in foo (elemAt itemsList 23)
# in foo (elemAt itemsList 24)
# in foo (elemAt itemsList 25)

