let
  inherit (builtins) add attrNames attrValues concatLists concatStringsSep elemAt genericClosure genList filter foldl' getAttr length listToAttrs mapAttrs mul toString trace zipAttrsWith;
  inherit (import ../utils.nix) euclidDiv isDigit isInt splitChar splitLines splitWords toInt;
in rec {

  parseItems = i: let
    it = let
      words = splitWords i;
    in genList (x: elemAt words (x + 2)) ((length words) - 2);
  in map (x: toInt (concatStringsSep "" (filter isDigit (splitChar x)))) it;

  parseOperation = o: old: let
    opDict = {
      "old" = old;
      "+" = add;
      "*" = mul;
    };
    op = let
      words = splitWords o;
    in genList (x: elemAt words (x + 3)) ((length words) - 3);
    elems = map (x: if isInt x then toInt x else getAttr x opDict) op;
  in (elemAt elems 1) (elemAt elems 0) (elemAt elems 2);

  parseTest = t: let
    te = let
      words = map splitWords t;
    in map (x: elemAt x ((length x) - 1)) words;
  in te;

  parseMonkey = n: m: {
    items = parseItems (elemAt m 1);
    operation = x: parseOperation (elemAt m 2) x;
    test = let
      values = parseTest (genList (x: elemAt m (x + 3)) 3);
    in x: let
      foo = x / (toInt (elemAt values 0));
    in if x == foo * (toInt (elemAt values 0)) then {"${elemAt values 1}" = x;} else {"${elemAt values 2}" = x;};
    # in x: if (euclidDiv x (toInt (elemAt values 0))).rem == 0 then {"${elemAt values 1}" = x;} else {"${elemAt values 2}" = x;};
    # in x: if (euclidDiv x (toInt (elemAt values 0))).rem == 0 then "${elemAt values 1}" else "${elemAt values 2}";
    # in x: let
    #   temp = x / (toInt (elemAt values 0));
    # in if temp == temp * (toInt (elemAt values 0)) then "${elemAt values 1}" else "${elemAt values 2}";
    name = n;
    inspectionCounter = 0;
  };

  parse = input: let
    lines = splitLines input;
    monkeyStrings = let
      getMonkey = offset: genList (m: elemAt lines (m + offset)) 6;
    in genList (m: getMonkey (m * 6)) ((length lines) / 6);
    monkeys = genList (x: {
      name = toString x;
      value = parseMonkey x (elemAt monkeyStrings x);
    }) (length monkeyStrings);
  in listToAttrs monkeys;

  inspect = x: operation: trace "inspecting ${toString x}" (operation x);
  # reduceWorry = x: trace "reducing worry off ${toString x}" x / 3;
  reduceWorry = x: let
    foo = x / 9699690;
    foobar = foo * 9699690;
  in trace "reducing worry off ${toString x}" (if x == foobar then foo else x - foobar);
  updateScores = monkey: let
    log = x: trace "Inspecting items: ${concatStringsSep " " (map toString monkey.items)}" x;
  in log (map (x: reduceWorry (inspect x monkey.operation)) monkey.items);

  throwItems = monkey: monkeys: let
    updatedScores = updateScores monkey;
    targets = zipAttrsWith (n: v: v) (map monkey.test updatedScores);
    logTargets = targets: x: let
      targetsString = t: map (i: toString i) (getAttr t targets);
      formatedTargets = map (t: "${t}: ${concatStringsSep " " (targetsString t)}") (attrNames targets);
    in trace
    ("Targets:\n" +
    (concatStringsSep "\n" formatedTargets))
    x;
    updateItems = m: items: (getAttr m monkeys).items ++ items;
  in logTargets targets (mapAttrs (n: v: (getAttr n monkeys) // {items = updateItems n v;}) targets);

  procTurn = monkeys: turn: let
    monkey = getAttr turn monkeys;
    emptyHandedMonkey = {
      ${turn} = monkey // {
        items = [];
        inspectionCounter = monkey.inspectionCounter + (length monkey.items);
      };
    };
    itemsThrown = trace "Monkeys has: ${concatStringsSep " " (map toString monkey.items)}" throwItems monkey monkeys;
  in trace "Turn of monkey: ${turn}" monkeys
    // emptyHandedMonkey
    // itemsThrown;

  procRound2 = monkeys: round: let
    turns = genericClosure {
      startSet = [{key = 0; monkeys = procTurn monkeys "0";}];
      operator = item: let
        newKey = if item.key == ((length (attrNames item.monkeys)) - 1) then 0 else item.key + 1;
      in [{
        key = newKey;
        monkeys = procTurn item.monkeys (toString newKey);
      }];
    };
  in trace "Processing round ${toString (round + 1)}" (elemAt turns ((length turns) - 1)).monkeys;

  procRound = monkeys: round: trace "Processing round: ${toString (round + 1)}" foldl' (x: y: procTurn x y) monkeys (attrNames monkeys);

  # item = v: m: { value = v; owners = [m]; };
  items = monkeys: concatLists (attrValues (mapAttrs (n: v: map (x: item x v monkeys) v.items) monkeys));

  item = v: m: monkeys: let
    getMonkey = m: getAttr m monkeys;
  in genericClosure {
    startSet = [{key = 0; value = v; monkey = m; round = 0;}];
    operator = item: let
      newKey = trace "Keying" (if item.round == 20 then 0 else item.key + 1);
      updatedValue = reduceWorry (inspect item.value item.monkey.operation);
      target = trace "Targetting" (item.monkey.test) updatedValue;
      updatedRound = (if item.monkey.name > toInt target then item.round + 1 else item.round);
    in trace "Sending ${toString updatedValue} to ${target} on round ${toString item.round}" [{ key = newKey; value = updatedValue; monkey = getMonkey target; round = updatedRound; }];
  };
}

