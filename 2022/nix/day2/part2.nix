{ input }:
let
  inherit (builtins) elem elemAt getAttr map;
  inherit (import ../utils.nix) sum splitLines splitWords;

  puzzle = map splitWords (splitLines input);

  score = let
    grid = {
      "X" = {
        "score" = 0;
        "shapes" = {
          "A" = 3;
          "B" = 1;
          "C" = 2;
        };
      };
      "Y" = {
        "score" = 3;
        "shapes" = {
          "A" = 1;
          "B" = 2;
          "C" = 3;
        };
      };
      "Z" = {
        "score" = 6;
        "shapes" = {
          "A" = 2;
          "B" = 3;
          "C" = 1;
        };
      };
    };
  in r: s: getAttr "score" (getAttr r grid) + getAttr s (getAttr "shapes" (getAttr r grid));

  scores = map (xs: score (elemAt xs 1) (elemAt xs 0)) puzzle;

in sum scores

