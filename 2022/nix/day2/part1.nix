{ input }:
let
  inherit (builtins) elem elemAt getAttr map;
  inherit (import ../utils.nix) sum splitLines splitWords;
  inherit (import ./utils.nix) parseInput;

  puzzle = splitLines input;

  score = let
    draws = [
      "A X"
      "B Y"
      "C Z"
    ];
    wins = [
      "A Y"
      "B Z"
      "C X"
    ];
    losses = [
      "A Z"
      "B Y"
      "C X"
    ];
    battleScore = xs: if elem xs wins then 6 else if elem xs draws then 3 else 0;
    shape = {
      "X" = 1;
      "Y" = 2;
      "Z" = 3;
    };
    shapeScore = s: getAttr s shape;
  in xs: battleScore xs + shapeScore (elemAt (splitWords xs) 1);

  scores = map score puzzle;

in sum scores

