let
  inherit (builtins) elemAt map;
  inherit (import ../utils.nix) splitLines splitX toInt;
in rec {
  parseInput = data: map (xs: map toInt (splitLines xs)) (splitX "\n\n" data);
}

