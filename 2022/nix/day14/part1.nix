{ input }:
let
  inherit (builtins) attrNames concatLists elem elemAt filter foldl' genericClosure genList getAttr groupBy head isList length map;
  inherit (import ../utils.nix) max min splitChar splitLines sum toInt;
  inherit (import ./utils.nix) genRocks genSand parse visualizeCave;

  puzzle = parse (splitLines input);

  rocks = concatLists (map genRocks puzzle);

  minX = foldl' min 9999 (map (x: elemAt x 0) rocks);
  maxX = foldl' max 0 (map (x: elemAt x 0) rocks);

  width = maxX - minX;
  depth = (foldl' max 0 (map (x: elemAt x 1) rocks)) + 1;
  abyss = filter (x: !(elem x rocks)) (genList (x: [((minX - 1) + x) (depth - 1)]) (width + 3));

  sand = genericClosure {
    startSet = [{ key = 0; cave = rocks; }];
    operator = item: let
      updatedCave = genSand item.cave abyss;
      nextKey = if isList updatedCave then item.key + 1 else 0;
    in [{ key = nextKey; cave = updatedCave; }];
  };

  end = let
    lastSand = groupBy (x: toString x.key) sand;
    sandCount = foldl' (x: y: max x (toInt y)) 0 (attrNames lastSand);
  in { cave = (elemAt (getAttr (toString sandCount) lastSand) 0).cave; sandCount = sandCount; };

# in puzzle
# in rocks
# in abyss
in end.sandCount
# in end.cave
# in visualizeCave rocks rocks width depth minX
# in visualizeCave rocks end.cave minX maxX

