let
  inherit (builtins) concatLists concatStringsSep elem elemAt filter foldl' genList getAttr length map trace;
  inherit (import ../utils.nix) abs max min splitWords splitX toInt;
in rec {

  parse = line: map (z: map (y: map toInt (splitX "," y)) (filter (x: x != "->") (splitWords z))) line;

  coords = x: { x = elemAt x 0; y = elemAt x 1; };
  rockLine = x: y: let
    a = coords x;
    b = coords y;
    dist = axis: (abs ((getAttr axis a) - (getAttr axis b)) + 1);
  in if a.x == b.x
  then genList (i: [a.x ((min a.y b.y) + i)]) (dist "y")
  else genList (i: [((min a.x b.x) + i) b.y]) (dist "x");

  genRocks = xs: concatLists (genList (x: rockLine (elemAt xs x) (elemAt xs (x + 1))) ((length xs) - 1));

  down = {x, y}: coords [x (y + 1)];
  downL = {x, y}: coords [(x - 1) (y + 1)];
  downR = {x, y}: coords [(x + 1) (y + 1)];
  blocked = {x, y}: cave: elem [x y] cave;
  abyssed = {x, y}: abyss: elem [x y] abyss;

  genSand = cave: abyss: let
    _innerMove = s: if abyssed s abyss
    then trace "Sand is falling into the abyss" false
    else if !(blocked (down s) cave)
    then trace "Sand is moving down" _innerMove (down s)
    else if !(blocked (downL s) cave)
    then trace "Sand is moving downL" _innerMove (downL s)
    else if !(blocked (downR s) cave)
    then trace "Sand is moving downR" _innerMove (downR s)
    else trace "Sand is stuck at ${(toString s.x)} ${(toString s.y)}" [[s.x s.y]] ++ cave;
  in _innerMove (coords [500 0]);

  visualizeCave = rocks: cave: width: depth: minX: let
    renderPoint = x: if elem x rocks then "#" else if elem x cave then "o" else ".";
  in concatStringsSep "\n" (genList (y: concatStringsSep "" (genList (x: renderPoint [(minX + x) y]) width)) depth);
}

