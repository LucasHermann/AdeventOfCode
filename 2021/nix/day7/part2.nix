{ input }:
let
  inherit (builtins) attrNames elemAt foldl' genList getAttr groupBy length;
  inherit (import ../utils.nix) abs incOrder splitLines splitX toInt;

  puzzle = splitX "," (elemAt (splitLines input) 0);
  grouped = groupBy (x: x) puzzle;
  initialPos = incOrder (attrNames grouped);
  posPos = genList (x: toString x) (toInt (elemAt initialPos ((length initialPos) - 1)));

  calcFuel = x: x * (x + 1) / 2;
  align = x: y: (calcFuel (abs ((toInt (elemAt x 0)) - y))) * length (x);
  getFuelCost = pos: foldl' (x: y: x + (align (getAttr y grouped) (toInt pos))) 0 initialPos;

  allRes = incOrder (map (x: getFuelCost x) posPos);
in elemAt allRes 0

