let
  inherit (builtins) elemAt bitAnd foldl' genList length toString;
  inherit (import ../utils.nix) abs euclidDiv round splitX toInt;
in rec {
  half = xs: (length xs) / 2;
  isEven = x: (bitAnd x 1) == 0;

  oddMedian = xs: h: elemAt xs h;
  evenMedian = xs: h: ((elemAt xs h) + (elemAt xs (h - 1))) / 2;
  median = xs: if isEven (length xs) then evenMedian xs (half xs) else oddMedian xs (half xs);

  mean = xs: round (euclidDiv (foldl' (x: y: x + y) 0 xs) (length xs));

}

