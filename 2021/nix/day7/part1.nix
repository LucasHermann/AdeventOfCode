{ input }:
let
  inherit (builtins) attrNames elemAt foldl' genList getAttr groupBy length;
  inherit (import ../utils.nix) abs incOrder splitLines splitX toInt;
  inherit (import ./utils.nix) median;

  puzzle = splitX "," (elemAt (splitLines input) 0);
  grouped = groupBy (x: x) puzzle;
  sorted = incOrder (map toInt puzzle);
  finalPos = median sorted;

  align = x: y: (abs ((toInt (elemAt x 0)) - y)) * length (x);

in foldl' (x: y: x + (align (getAttr y grouped) finalPos)) 0 (attrNames grouped)

