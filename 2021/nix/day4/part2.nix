{ input }:
let
  inherit (builtins) elemAt filter genList head length tail;
  inherit (import ../utils.nix) splitLines splitX;
  inherit (import ./utils.nix) getBoard mark score win;

  puzzle = splitLines input;
  numbers = splitX "," (head puzzle);
  boardsInput = tail puzzle;
  boards = genList (x: getBoard x boardsInput) ((length boardsInput) / 5);

  bingo = bs: i: if length bs == 1 then { num = elemAt numbers i; board = mark (elemAt numbers i) (elemAt bs 0); } else bingo (filter (x: !win x) (map (b: mark (elemAt numbers i) b) bs)) (i + 1);

in score (bingo boards 0)

