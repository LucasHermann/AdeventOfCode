{ input }:
let
  inherit (builtins) any elemAt filter genList head length tail;
  inherit (import ../utils.nix) splitLines splitX;
  inherit (import ./utils.nix) getBoard mark score win;

  puzzle = splitLines input;
  numbers = splitX "," (head puzzle);
  boardsInput = tail puzzle;
  boards = genList (x: getBoard x boardsInput) ((length boardsInput) / 5);

  bingo = bs: i: if any win bs then { num = elemAt numbers (i - 1); board = elemAt (filter win bs) 0; } else bingo (map (b: mark (elemAt numbers i) b) bs) (i + 1);
in score (bingo boards 0)

