let
  inherit (builtins) any elemAt filter foldl' genList length;
  inherit (import ../utils.nix) toInt splitWords;
in rec {
  getRows = i: xs: genList (x: splitWords (elemAt xs ((i * 5) + x))) 5;
  getColumns = xs: xs ++ (genList (x: genList (y: elemAt (elemAt xs y) x) 5) 5);
  getBoard = i: xs: getColumns (getRows i xs);

  mark = n: board: map (line: filter (x: x!= n) line) board;
  win = board: any (x: (length x) == 0) board;

  score = { num, board }: (toInt num) * ((foldl' (x: y: x + foldl' (xx: yy: xx + (toInt yy)) 0 y) 0 board) / 2);
}
