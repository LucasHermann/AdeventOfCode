{ input ? builtins.readFile ./input.txt }:
let
  inherit (builtins) elemAt filter foldl' genList length replaceStrings toString;
  inherit (import ../utils.nix) toIntFromBit splitChar splitLines;

  puzzle = map(xs: splitChar xs) (splitLines input);

  calc_occurence = xs: ys: genList (i: if (elemAt ys i) == "1" then ((elemAt xs i) + 1) else ((elemAt xs i) - 1)) (length xs);

  base = (genList (x: 0) (length (elemAt puzzle 0)));
  format = xs: (replaceStrings [" "] [""] (toString xs));
  reduce = calc: toIntFromBit (format ((map (x: calc x) (foldl' (xs: ys: calc_occurence xs ys ) base puzzle))));

  gamma = x: if x > 0 then "1" else "0";
  epsilon = x: if x < 0 then "1" else "0";

in reduce gamma * reduce epsilon

