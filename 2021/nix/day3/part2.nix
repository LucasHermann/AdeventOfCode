{ input ? builtins.readFile ./input.txt }:
let
  inherit (builtins) elemAt filter foldl' genList length replaceStrings toString;
  inherit (import ../utils.nix) toIntFromBit splitChar splitLines;

  puzzle = map(xs: splitChar xs) (splitLines input);

  getVal = xs: toIntFromBit (replaceStrings [" "] [""] (toString xs));
  mostCommon = xs: i: foldl' (x: ys: if (elemAt ys i) == "1" then x + 1 else x - 1) 0 xs;
  reduce = pos: xs: filt: if length xs > 1 then reduce (pos + 1) (filt xs (mostCommon xs pos) pos) filt else xs;
  rating = filt: getVal (reduce 0 puzzle filt);

  oxyGen = xs: c: i: filter (x: if c >= 0 then elemAt x i == "1" else elemAt x i == "0") xs;
  co2Scrub = xs: c: i: filter (x: if c < 0 then elemAt x i == "1" else elemAt x i == "0") xs;


in rating oxyGen * rating co2Scrub

