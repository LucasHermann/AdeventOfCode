{ input }:
let
  inherit (builtins) attrNames elemAt foldl' filter genList getAttr groupBy hasAttr length throw toString tryEval;
  inherit (import ../utils.nix) abs incOrder splitChar splitLines splitWords splitX toInt;
  inherit (import ./utils.nix) median;

  segToInt = segs: let
    segLen = toString (length (splitChar segs));
    segLengthDict = {
      "2" = 1;
      "4" = 4;
      "3" = 7;
      "7" = 8;
    };
  in if hasAttr segLen segLengthDict then getAttr segLen segLengthDict else throw "Can't infer value from segment length";

  puzzle = map (splitX "[|]") (splitLines input);
  grouped = map (x: { "sigPat" = splitWords (elemAt x 0); "outVal" = splitWords (elemAt x 1); }) puzzle;

  parsed = map (x: map (y: tryEval (segToInt y)) x.outVal) grouped;

in foldl' (x: y: x + length y) 0 (map (filter (x: x.success)) parsed)

