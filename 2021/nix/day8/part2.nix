{ input }:
let
  inherit (builtins) all elem elemAt foldl' filter genList getAttr length listToAttrs toString;
  inherit (import ../utils.nix) incOrder splitChar splitLines splitWords splitX;

  lines = map (splitX "[|]") (splitLines input);
  puzzle = map (x: { "sigPat" = map splitChar (splitWords (elemAt x 0)); "outVal" = map splitChar (splitWords (elemAt x 1)); }) lines;

  genSigToSegDict = sigPat: let
    segs = [ "a" "b" "c" "d" "e" "f" "g" ];
    remSeg = segs: seg: filter (x: x != seg) segs;
    ret = res: l: segs: seg: { "res" = res // { "${seg}" = l; }; "segs" = remSeg segs seg; };
    oneSigs = elemAt (filter (x: length x == 2) sigPat) 0;
    fourSigs = elemAt (filter (x: length x == 4) sigPat) 0;
    sevenSigs = elemAt (filter (x: length x == 3) sigPat) 0;
    heightSigs = elemAt (filter (x: length x == 7) sigPat) 0;
    twoThreefive = filter (x: length x == 5) sigPat;
    zeroSixNine = filter (x: length x == 6) sigPat;
    a = let
      seg = elemAt (filter (x: !(elem x oneSigs) && (elem x sevenSigs)) segs) 0;
    in ret {} "a" segs seg;
    b = { res, segs }: let
      seg = elemAt (filter (x: (elem x fourSigs) && !(res ? "${x}")) segs) 0;
    in ret res "b" segs seg;
    c = { res, segs }: let
      seg = elemAt (filter (x: (elem x oneSigs) && !(all (y: elem x y) zeroSixNine)) segs) 0;
    in ret res "c" segs seg;
    d = { res, segs }: let
      seg = elemAt (filter (x: (all (y: elem x y) twoThreefive) && (elem x fourSigs)) segs) 0;
    in ret res "d" segs seg;
    e = { res, segs }: let
      seg = elemAt segs 0;
    in ret res "e" segs seg;
    f = { res, segs }: let
      seg = elemAt (filter (x: (elem x oneSigs) && !(res ? "${x}")) segs) 0;
    in ret res "f" segs seg;
    g = { res, segs }: let
      seg = elemAt (filter (x: (all (y: elem x y) zeroSixNine) && !(res ? "${x}")) segs) 0;
    in ret res "g" segs seg;
  in (e (g (b (d (f (c a)))))).res;

  translate = sigPat: val: let
    transTable = genSigToSegDict sigPat;
  in toString (incOrder (map (x: transTable."${x}") val));

  sigsToVal = { sigPat, outVal }: let
    correctedSig = map (x: translate sigPat x) outVal;
    segToValDict = {
      "a b c e f g" = 0;
      "c f" = 1;
      "a c d e g" = 2;
      "a c d f g" = 3;
      "b c d f" = 4;
      "a b d f g" = 5;
      "a b d e f g" = 6;
      "a c f" = 7;
      "a b c d e f g" = 8;
      "a b c d f g" = 9;
    };
  in map (x: getAttr x segToValDict) correctedSig;

  listToInt = l: foldl' (x: y: x * 10 + y) 0 l;

in foldl' (x: y: x + (listToInt y)) 0 (map (sigsToVal) puzzle)

