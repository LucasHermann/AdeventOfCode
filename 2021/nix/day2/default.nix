let
  part1 = import ./part1.nix {};
  part2 = import ./part2.nix {};
in {
  inherit part1;
  inherit part2;
}
