{ input ? builtins.readFile ./input.txt }:
let
  inherit (builtins) elemAt catAttrs filter foldl' genList length;
  inherit (import ../utils.nix) toInt splitChar splitLines splitWords;

  input_lines = splitLines input;
  input_words = map (splitWords) input_lines;
  puzzle = map (x: {${elemAt x 0} = (toInt (elemAt x 1)); }) input_words;

  reduce = x: foldl' (x: y: x + y) 0 (catAttrs x puzzle);
  forward = reduce "forward";
  up = reduce "up";
  down = reduce "down";

in forward * (down - up)

