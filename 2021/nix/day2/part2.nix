{ input ? builtins.readFile ./input.txt }:
let
  inherit (builtins) elemAt catAttrs filter foldl' genList length;
  inherit (import ../utils.nix) toInt splitChar splitLines splitWords;

  input_lines = splitLines input;
  input_words = map (splitWords) input_lines;
  puzzle = map (x: {dir = (elemAt x 0); val = (toInt (elemAt x 1)); }) input_words;

  down = i: x: {aim = (i.aim + x); dist = i.dist; depth = i.depth;};
  up = i: x: {aim = (i.aim - x); dist = i.dist; depth = i.depth;};
  forward = i: x: {aim = i.aim; dist = i.dist + x; depth = i.depth + (i.aim * x);};

  res = foldl' (x: y: if y.dir == "down" then down x y.val else if y.dir == "up" then up x y.val else forward x y.val) {aim = 0; dist = 0; depth = 0;} puzzle;

in res.depth * res.dist

