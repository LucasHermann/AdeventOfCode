{ input }:
let
  inherit (builtins) any attrNames elem elemAt filter foldl' genList getAttr groupBy head length tail toString;
  inherit (import ../utils.nix) abs min splitLines splitX toInt;

  coords = map (splitX " -> ") (splitLines input);
  puzzle = map (map (x: map toInt (splitX "," x))) coords;

  line = f: x: y: genList f (abs (x - y) + 1);
  move = i: x: y: if (x - y) > 0 then x - i else x + i;
  horizontal = x: y: line (i: [(elemAt x 0) (i + (min (elemAt x 1) (elemAt y 1)))]) (elemAt x 1) (elemAt y 1);
  vertical = x: y: line (i: [(i + (min (elemAt x 0) (elemAt y 0))) (elemAt x 1)]) (elemAt x 0) (elemAt y 0);
  diagonal = x: y: line (i: [(move i (elemAt x 0) (elemAt y 0)) (move i (elemAt x 1) (elemAt y 1))]) (elemAt x 0)  (elemAt y 0);

  hydrotermalVent = x: y: if (elemAt x 0) == (elemAt y 0) then horizontal x y else if (elemAt x 1) == (elemAt y 1) then vertical x y else diagonal x y;

  ventMap = map (x: hydrotermalVent (elemAt x 0) (elemAt x 1)) puzzle;
  flatten = xs: foldl' (x: y: x ++ y) [] xs;
  count = xs: groupBy toString (flatten xs);
  danger = xs: filter (x: length (getAttr x xs) > 1) (attrNames xs);

in length (danger (count ventMap))

