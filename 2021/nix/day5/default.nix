{ input ? ./input.txt }:
let
  inherit (builtins) readFile;

in {
  part1 = import ./part1.nix { input = (readFile input); };
  part2 = import ./part2.nix { input = (readFile input); };
}

