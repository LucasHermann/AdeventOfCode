let
  inherit (builtins) elemAt foldl' genList length toString;
  inherit (import ../utils.nix) splitX;
in rec {
  base = { "0" = []; "1" = []; "2" = []; "3" = []; "4" = []; "5" = []; "6" = []; "7" = []; "8" = []; };

  age = xs: i: if i == 6 then (elemAt xs 0) + (elemAt xs 7) else if i == 8 then elemAt xs 0  else elemAt xs (i + 1);
  day = xs: genList (x: age xs x) 9;
  runFor = xs: d: foldl' (x: y: day x) xs (genList (x:x) d);

  sum = xs: foldl' (x: y: x + y) 0 xs;
}

