{ input }:
let
  inherit (builtins) elemAt genList getAttr groupBy length;
  inherit (import ../utils.nix) splitLines splitX;
  inherit (import ./utils.nix) base runFor sum;

  temp = base // (groupBy (x: x) (splitX "," (elemAt (splitLines input) 0)));
  puzzle = genList (x: length (getAttr (toString x) temp)) 9;

in sum (runFor puzzle 80)

