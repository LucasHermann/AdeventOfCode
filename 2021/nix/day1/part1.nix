{ input ? import ./input.nix }:
let
  inherit (builtins) elemAt foldl' genList length;
  inherit (import ./utils.nix) comp inc win;
  input = import ./input.nix;

  form_input = win input 2;

  part1 = foldl' (x: y: x + (comp inc (elemAt y 0) (elemAt y 1))) 0 form_input;
in {
  inherit part1;
}
