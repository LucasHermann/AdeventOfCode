let
  inherit (builtins) elemAt genList length;
in rec {
  innerWin = xs: s: c: genList (i: elemAt xs (s + i)) c;
  win = xs: c: genList (x: innerWin xs x c) ((length xs) - ( c - 1));

  inc = (l: r: l < r);
  dec = (l: r: l > r);
  comp = f: x: y: if f x y then 1 else 0;
}

