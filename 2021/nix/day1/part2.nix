{ input ? import ./input.nix }:
let
  inherit (builtins) elemAt foldl' genList length map;
  inherit (import ./utils.nix) comp inc win;

  form_input = map (x: foldl' (x: y: x + y) 0 x) (win input 3);

  part2 = foldl' (x: y: x + (comp inc (elemAt y 0) (elemAt y 1))) 0 (win form_input 2);
in {
  inherit part2;
}
