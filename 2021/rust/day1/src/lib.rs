fn calculate_depth_increase(measurements: &[usize]) -> usize {
    measurements.windows(2).filter(|&pair| pair[0] < pair[1]).count()
}

fn calculate_gneralized_depth_increase(measurements: &[usize]) -> usize {
    measurements.windows(3).zip(measurements.windows(3).skip(1)).filter(|&(prev, next)| prev.iter().sum::<usize>() < next.iter().sum::<usize>()).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: [usize; 10] = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
    const INPUT: &str = include_str!("input.txt");

    #[test]
    fn example_part_one() {
        let result = calculate_depth_increase(&EXAMPLE[..]);
        assert_eq!(result, 7);
    }

   #[test]
    fn resolve_part_one() {
        let puzzle: Vec<usize> = INPUT.lines().map(|x| x.parse::<usize>().unwrap()).collect();
        let result = calculate_depth_increase(&puzzle[..]);
        assert_eq!(result, 1184);
    }

    #[test]
    fn example_part_two() {
        let result = calculate_gneralized_depth_increase(&EXAMPLE[..]);
        assert_eq!(result, 5);
    }

   #[test]
    fn resolve_part_two() {
        let puzzle: Vec<usize> = INPUT.lines().map(|x| x.parse::<usize>().unwrap()).collect();
        let result = calculate_gneralized_depth_increase(&puzzle[..]);
        assert_eq!(result, 1158);
    }
}

