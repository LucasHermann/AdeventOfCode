use std::convert::TryFrom;

#[derive(Debug, PartialEq)]
enum PassportField {
    Byr(Vec<char>),
    Iyr(Vec<char>),
    Eyr(Vec<char>),
    Hgt(Vec<char>),
    Hcl(Vec<char>),
    Ecl(Vec<char>),
    Pid(Vec<char>),
    Cid(Vec<char>),
}

impl TryFrom<&str> for PassportField {
    type Error = &'static str;
    fn try_from(input:&str) -> Result<PassportField, Self::Error> {
        let field = input.chars().take(3).collect::<String>();
        match field.as_ref() {
            "byr" => Ok(PassportField::Byr(input.chars().skip(4).collect())),
            "iyr" => Ok(PassportField::Iyr(input.chars().skip(4).collect())),
            "eyr" => Ok(PassportField::Eyr(input.chars().skip(4).collect())),
            "hgt" => Ok(PassportField::Hgt(input.chars().skip(4).collect())),
            "hcl" => Ok(PassportField::Hcl(input.chars().skip(4).collect())),
            "ecl" => Ok(PassportField::Ecl(input.chars().skip(4).collect())),
            "pid" => Ok(PassportField::Pid(input.chars().skip(4).collect())),
            "cid" => Ok(PassportField::Cid(input.chars().skip(4).collect())),
            _ => Err("Unrecognised filed type"),
        }
    }
}

#[derive(Debug, PartialEq)]
struct Passport {
    byr: Option<PassportField>,
    iyr: Option<PassportField>,
    eyr: Option<PassportField>,
    hgt: Option<PassportField>,
    hcl: Option<PassportField>,
    ecl: Option<PassportField>,
    pid: Option<PassportField>,
    cid: Option<PassportField>,
}

impl Passport {
    fn new() -> Self {
        Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
        }
    }
}

impl From<&str> for Passport {
    fn from(input: &str) -> Passport {
        let input = input.split(' ').filter_map(|f| PassportField::try_from(f).ok());
        let mut pass = Passport::new();

        for field in input {
            match field {
                PassportField::Byr(ref Vec) => pass.byr = Some(field),
                PassportField::Iyr(ref Vec) => pass.iyr = Some(field),
                PassportField::Eyr(ref Vec) => pass.eyr = Some(field),
                PassportField::Hgt(ref Vec) => pass.hgt = Some(field),
                PassportField::Hcl(ref Vec) => pass.hcl = Some(field),
                PassportField::Ecl(ref Vec) => pass.ecl = Some(field),
                PassportField::Pid(ref Vec) => pass.pid = Some(field),
                PassportField::Cid(ref Vec) => pass.cid = Some(field),
            };
        };
        pass
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn passport_field_from_char() {
        let input = [
            "ecl:gry",
            "pid:860033327",
            "eyr:2020",
            "hcl:#fffffd",
            "byr:1937",
            "iyr:2017",
            "cid:147",
            "hgt:183cm",
        ];
        let expected = [
            PassportField::Ecl("gry".chars().collect()),
            PassportField::Pid("860033327".chars().collect()),
            PassportField::Eyr("2020".chars().collect()),
            PassportField::Hcl("#fffffd".chars().collect()),
            PassportField::Byr("1937".chars().collect()),
            PassportField::Iyr("2017".chars().collect()),
            PassportField::Cid("147".chars().collect()),
            PassportField::Hgt("183cm".chars().collect()),
        ];

        let results = input.iter().filter_map(|i| PassportField::try_from(*i).ok());
        expected.iter().zip(results).map(|(e, r)| assert_eq!(*e, r));
    }

    #[test]
    fn passport_from_string() {
        let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm";
        let mut expected = Passport::new();
        expected.ecl = Some(PassportField::Ecl("gry".chars().collect()));
        expected.pid = Some(PassportField::Pid("860033327".chars().collect()));
        expected.eyr = Some(PassportField::Eyr("2020".chars().collect()));
        expected.hcl = Some(PassportField::Hcl("#fffffd".chars().collect()));
        expected.byr = Some(PassportField::Byr("1937".chars().collect()));
        expected.iyr = Some(PassportField::Iyr("2017".chars().collect()));
        expected.cid = Some(PassportField::Cid("147".chars().collect()));
        expected.hgt = Some(PassportField::Hgt("183cm".chars().collect()));

        let result = Passport::from(input);

        assert_eq!(expected, result);
    }
}
