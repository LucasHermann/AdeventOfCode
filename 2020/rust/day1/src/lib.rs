fn part_one(expenses: Vec<usize>) -> usize {
    for (index, candidate) in expenses.iter().enumerate() {
        for expense in expenses[index+1..].iter() {
            if candidate + expense == 2020 {
                return candidate * expense;
            }
        }
    }

    return 1
}

fn part_two(expenses : Vec<usize>) -> usize {
    for (index, candidate) in expenses.iter().enumerate() {
        for (sec_index, sec) in expenses[index+1..].iter().enumerate() {
            for expense in expenses[sec_index+1..].iter() {
                if candidate + sec + expense == 2020 {
                    return candidate * sec * expense;
                }
            }
        }
    }

    return 1

}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn example_part_one() {
        let mut expenses = Vec::new();

        expenses.push(1721);
        expenses.push(979);
        expenses.push(366);
        expenses.push(299);
        expenses.push(675);
        expenses.push(1456);

        let result = part_one(expenses);
        assert_eq!(result, 514579);
    }

    #[test]
    fn resolve_part_one() {
        const INPUT: &str = include_str!("input.txt");

        let puzzle = INPUT.lines().map(|x| x.parse::<usize>().unwrap()).collect();
        let result = part_one(puzzle);

        assert_eq!(result, 436404);
    }

    #[test]
    fn example_part_two() {
        let mut expenses = Vec::new();

        expenses.push(1721);
        expenses.push(979);
        expenses.push(366);
        expenses.push(299);
        expenses.push(675);
        expenses.push(1456);

        let result = part_two(expenses);
        assert_eq!(result, 241861950);
    }

    #[test]
    fn resolve_part_two() {
        const INPUT: &str = include_str!("input.txt");

        let puzzle = INPUT.lines().map(|x| x.parse::<usize>().unwrap()).collect();
        let result = part_two(puzzle);

        assert_eq!(result, 274879808);
    }
}
