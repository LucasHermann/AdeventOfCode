use regex::Regex;
use std::convert::TryFrom;

/// Pasword entry as sotred in the elf's database
///
/// Examples given:
///     1-3 a: abcde
///     1-3 b: cdefg
///     2-9 c: ccccccccc
struct PassDbEntry {
    policy_low: usize,
    policy_up: usize,
    letter: char,
    password: String,
}

impl PassDbEntry {
    fn new(policy_low: usize, policy_up: usize, letter: char, password: String) -> Self {
        PassDbEntry{policy_low, policy_up, letter, password}
    }

    fn parse_using_regex(entry: &str) -> Result<PassDbEntry, Box<dyn std::error::Error>> {
        let re = Regex::new(r"^(?P<low>\d+)-(?P<up>\d+) (?P<lett>\w{1}): (?P<pass>\w+)$").unwrap();
        let cap = re.captures(entry).unwrap();
        let groups = (cap.name("low"), cap.name("up"), cap.name("lett"), cap.name("pass"));
        match groups {
            (Some(low), Some(up), Some(lett), Some(pass)) => Ok(PassDbEntry {
                policy_low: low.as_str().parse().unwrap(),
                policy_up: up.as_str().parse().unwrap(),
                letter: lett.as_str().to_string().remove(0),
                password: pass.as_str().to_string(),
            }),
            _ => Err(From::from("Invalid entry")),
        }
    }

    fn parse_smarter(entries: &str) -> Result<PassDbEntry, Box<dyn std::error::Error>> {
        let mut entries_iter = entries.chars();
        Ok(PassDbEntry {
            policy_low: entries_iter.by_ref().take_while(|&c| c != '-').collect::<String>().parse().unwrap(),
            policy_up: entries_iter.by_ref().take_while(|&c| c != ' ').collect::<String>().parse().unwrap(),
            letter: entries_iter.next().unwrap(),
            password: entries_iter.skip(2).collect(),
        })
    }

    fn is_valid(&self) -> bool {
        if self.policy_low > self.password.len() {
            return false
        }
        let occ = self.password.matches(self.letter).count();
        if self.policy_low <= occ && occ <= self.policy_up {
            return true
        }
        else {
            return false
        }
    }

    fn toboggan_pol(&self) -> bool {
        let low_let: char = self.password.as_bytes()[self.policy_low - 1] as char;
        let up_let: char = self.password.as_bytes()[self.policy_up - 1] as char;
        (low_let == self.letter) ^ (up_let == self.letter)
    }
}

impl TryFrom<&str> for PassDbEntry {
    type Error = Box<dyn std::error::Error>;

    fn try_from(entry: &str) -> Result<PassDbEntry, Box<dyn std::error::Error>> {
        PassDbEntry::parse_smarter(entry)
    }

}

fn solve_one(entries: Vec<PassDbEntry>) -> usize {
    entries.iter().filter(|e| e.is_valid()).count()
}

fn solve_two(entries: Vec<PassDbEntry>) -> usize {
    entries.iter().filter(|e| e.toboggan_pol()).count()
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = include_str!("input.txt");

    #[test]
    fn convert_inpt_to_passdbentry() {
        let result = PassDbEntry::try_from("1-3 a: abcde");
        assert!(result.is_ok());
    }

    #[test]
    fn is_invalid() {
        let result = PassDbEntry::try_from("1-3 a: abcde");
        assert!(result.is_ok());
        let result = result.unwrap();
        assert!(result.is_valid());

        let result = PassDbEntry::try_from("1-3 b: cdefg");
        assert!(result.is_ok());
        let result = result.unwrap();
        assert!(!result.is_valid());
    }

    #[test]
    fn example_one() {
        let entry = [
            "1-3 a: abcde",
            "1-3 b: cdefg",
            "2-9 c: ccccccccc",
        ];
        let puzzle = entry.iter().filter_map(|e| PassDbEntry::try_from(*e).ok()).collect();

        let result = solve_one(puzzle);

        assert_eq!(result, 2);
    }

    #[test]
    fn extra_tests() {
        let entry = [
            "1-3 a: aajflsdjf",
            "1-2 a: aajflsdjf",
            "10-13 a: aajflaaaaaasdaaajf",
            "4-14 d: ddddddhdddddcbddd",
            "3-4 c: cccc",
        ];
        let puzzle = entry.iter().filter_map(|e| PassDbEntry::try_from(*e).ok()).collect();

        let result = solve_one(puzzle);

        assert_eq!(result, 5);
    }

    #[test]
    fn resolve_part_one() {

        let puzzle = INPUT.lines().filter_map(|e| PassDbEntry::try_from(e).ok()).collect();

        let result = solve_one(puzzle);

        assert_eq!(result, 445);
    }

    #[test]
    fn example_two() {
        let entry = [
            "1-3 a: abcde",
            "1-3 b: cdefg",
            "2-9 c: ccccccccc",
        ];
        let puzzle = entry.iter().filter_map(|e| PassDbEntry::try_from(*e).ok()).collect();

        let result = solve_two(puzzle);

        assert_eq!(result, 1);
    }

    #[test]
    fn more_tests_two() {
        let entry = [
            "1-3 a: aacaa",
            "1-3 a: caaaa",
            "1-3 b: cbebb",
            "10-13 a: aaaaaaaaaaaabaaaaa",
            "10-13 a: aaaaaaaaabaaaaaaaa",
        ];
        let puzzle = entry.iter().filter_map(|e| PassDbEntry::try_from(*e).ok()).collect();

        let result = solve_two(puzzle);

        assert_eq!(result, 4);
    }

    #[test]
    fn resolve_part_two() {

        let puzzle = INPUT.lines().filter_map(|e| PassDbEntry::try_from(e).ok()).collect();

        let result = solve_two(puzzle);

        // 490 to low
        // 510 to high
        assert_eq!(result, 445);
    }
}
