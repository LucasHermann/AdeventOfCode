use std::convert::TryFrom;

#[derive(PartialEq, Debug)]
enum Cell {
    Open,
    Tree,
}

impl TryFrom<char> for Cell {
    type Error = &'static str;

    fn try_from(c: char) -> Result<Cell, Self::Error> {
        match c {
            '.' => Ok(Cell::Open),
            '#' => Ok(Cell::Tree),
            _ => Err("Unsupported character"),
        }
    }
}

impl Cell {
    fn traverse(&self) -> usize {
        match self {
            Cell::Open => 0,
            Cell::Tree => 1,
        }
    }
}

fn parse_line(line: &str, step: usize) -> impl Iterator<Item=Cell> + '_ {
    line.chars().filter_map(|c| Cell::try_from(c).ok()).cycle().step_by(step)
}

fn solve_part_one<'a>(lines: impl Iterator<Item=&'a str>) -> usize {
    lines.map(|l| parse_line(l, 3)).enumerate().fold(0, |acc, mut l| acc + l.1.nth(l.0).unwrap().traverse())
}

fn solve_part_two_part<'a>(lines: impl Iterator<Item=&'a str>, step: usize) -> usize {
    lines.map(|l| parse_line(l, step)).enumerate().fold(0, |acc, mut l| acc + l.1.nth(l.0).unwrap().traverse())
}

fn solve_part_two_end<'a>(lines: impl Iterator<Item=&'a str>, step: usize) -> usize {
    lines.map(|l| parse_line(l, 1)).step_by(step).enumerate().fold(0, |acc, mut l| acc + l.1.nth(l.0).unwrap().traverse())
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = include_str!("input.txt");

    #[test]
    fn cell_from_char() {
        assert_eq!(Ok(Cell::Open), Cell::try_from('.'));
        assert_eq!(Ok(Cell::Tree), Cell::try_from('#'));
        assert_eq!(Err("Unsupported character"), Cell::try_from('a'));
    }

    #[test]
    fn read_line() {
        let input = "..##.......";
        let expected = vec![Cell::Open, Cell::Open, Cell::Tree, Cell::Tree, Cell::Open, Cell::Open, Cell::Open, Cell::Open, Cell::Open, Cell::Open, Cell::Open];

        let mut result = parse_line(input, 1);

        expected.iter().for_each(|c| assert_eq!(*c, result.next().unwrap()));
    }

    #[test]
    fn example_one() {
        let input = [
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#",
        ];

        assert_eq!(7, solve_part_one(input.iter().map(|e| *e)));
    }

    #[test]
    fn part_one() {
        assert_eq!(284, solve_part_one(INPUT.lines()));
    }

    #[test]
    fn example_two() {
        let input = [
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#",
        ];

        let partial = [1,3,5,7].iter().map(|s| solve_part_two_part(input.iter().map(|e| *e), *s)).fold(1, |acc, t| acc * t);
        let result = partial * solve_part_two_end(input.iter().map(|e| *e), 2);
        assert_eq!(336, result);
    }

    #[test]
    fn part_two() {
        let partial = [1,3,5,7].iter().map(|s| solve_part_two_part(INPUT.lines().map(|e| e), *s)).fold(1, |acc, t| acc * t);
        let result = partial * solve_part_two_end(INPUT.lines().map(|e| e), 2);
        assert_eq!(3510149120, result);
    }
}
