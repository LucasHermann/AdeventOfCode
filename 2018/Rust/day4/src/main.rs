use std::collections::BTreeMap;
const PUZZLE : &str = include_str!("input.txt");

struct Node {
    children: BTreeMap<u16, Node>,
    data: Vec<u8>
}

fn main() {
    for line in PUZZLE.lines() {
        let input: Vec<&str> = line.split(|c|
                                    c == '[' ||
                                    c == ']' ||
                                    c == '-' ||
                                    c == ':' ||
                                    c == ' ')
                 .collect();
        println!("{:?}", input);
    }
}
